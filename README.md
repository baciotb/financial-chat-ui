# FinancialChatUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Getting started
Run `npm i` to install dependencies

## Development server

Run `npx ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
