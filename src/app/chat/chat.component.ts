import { Component, OnInit, OnDestroy } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SocketJwt } from '../services/socket-jwt.service';

let io: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [SocketJwt, SocketService]
})
export class ChatComponent implements OnInit, OnDestroy {
  socket;
  messages = [];
  message: string = '';
  messageSub: Subscription;
  constructor(
    private socketService: SocketService,
    private router: Router
  ) { }

  ngOnInit() {
    const user = localStorage.getItem('fc-user');
    if (!user) {
      this.router.navigate(['/login']);
    }
    this.messageSub = this.socketService.messages.subscribe(message => {
      this.messages.push(message);
      this.messages.sort((a, b) => a.timestamp - b.timestamp);
      this.messages.slice(Math.max(this.messages.length - 50, 0));
    });
  }

  ngOnDestroy() {
    this.messageSub.unsubscribe();
  }

  sendMessage() {
    const user = JSON.parse(localStorage.getItem('fc-user'));
    this.socketService.sendMessage({msg: this.message, from: user.username});
    this.message = '';
  }

  logout() {
    localStorage.clear();
    location.reload(true);
  }

}
