import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router
  ) {}
  title = 'www';

  ngOnInit() {
    try {
      const token = localStorage.getItem('fc-user');
      if (token) {
        this.router.navigate(['/chat']);
      } else {
        this.router.navigate(['/login']);
      }
    } catch {}
  }
}
