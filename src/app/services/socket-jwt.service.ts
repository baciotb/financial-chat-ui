import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable()
export class SocketJwt extends Socket {

    constructor() {
        debugger;
        const {token} = JSON.parse(localStorage.getItem('fc-user'));
        super({ url: 'http://localhost:3000', options: {query: {token}} });
    }

}
