import { Injectable } from '@angular/core';
import { SocketJwt } from './socket-jwt.service';

@Injectable()
export class SocketService {
  messages = this.socket.fromEvent<{msg: string, from: string}[]>('chat message');

  constructor(private socket: SocketJwt) { }

  sendMessage(msg: {msg: string, from: string}) {
    this.socket.emit('chat message', {...msg, timestamp: new Date().getTime()});
  }
}