import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  newUsername: string;
  newPassword: string;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  ngOnInit() {
    localStorage.setItem('fc-chat', '');
  }

  login() {
    this.http.post('http://localhost:3000/api/auth/login', {
      username: this.username,
      password: this.password
    }).subscribe(response => {
      localStorage.setItem('fc-user', JSON.stringify({...response, username: this.username}));
      this.router.navigate(['/chat']);
    });
  }

  register() {
    this.http.post('http://localhost:3000/api/users/register', {
      username: this.newUsername,
      password: this.newPassword
    }).subscribe(() => {
      this.newUsername = '';
      this.newPassword = '';
    });
  }

}
